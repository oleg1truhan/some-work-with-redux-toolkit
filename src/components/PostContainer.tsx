import React, {useEffect, useState} from 'react';
import {postApi} from '../service/PostService';
import PostITem from './PostITem';
import {IPost} from '../models/IPost';

const PostContainer = () => {
    const [limit, setLimit] = useState(100);
    const {data: posts, error, isLoading, refetch} = postApi.useFetchAllPostsQuery(limit);
    const [createPost, {}] = postApi.useCreatePostMutation();
    const [updatePost, {}] = postApi.useUpdatePostMutation();
    const [deletePost, {}] = postApi.useDeletePostMutation();

    useEffect(() => {
        // setInterval(() => setLimit(3), 3000)
    }, []);

    const handleCreate = async () => {
        const title = prompt();
        await createPost({title, body: title} as IPost);
    };

    return (
        <div>
            <div className='post__list'>
                <button onClick={handleCreate}>Add new post</button>
                {isLoading && <h1>Loading...</h1>}
                {error && <h1>Some error occurred</h1>}
                {posts && posts.map(p =>
                    <PostITem
                        key={p.id}
                        post={p}
                        remove={deletePost}
                        update={updatePost}/>
                )}
            </div>
        </div>
    );
};

export default PostContainer;
