import React, {useEffect} from 'react';
import {useAppASelector, useAppDispatch} from './hooks/redux';
import {fetchUsers} from './store/reducers/ActionCreators';
import PostContainer from './components/PostContainer';

function App() {

    const dispatch = useAppDispatch();
    const {users, isLoading, error} = useAppASelector(state => state.userReducer);
    useEffect(() => {
        dispatch(fetchUsers());
    }, []);

    return (
        <div className="App">
            <PostContainer/>
            {/*{isLoading && <h1>Loading...</h1>}*/}
            {/*{error && <h1>{error}</h1>}*/}
            {/*{JSON.stringify(users, null, 2)}*/}
        </div>
    );
}

export default App;
